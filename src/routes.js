import React from 'react'

// Users
const Users = React.lazy(() => import('./views/users/index'))
const UserDetails = React.lazy(() => import('./views/UserDetails/index'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/users', name: 'Users', component: Users },

  { path: '/user-details/:id', name: 'User Details', component: UserDetails },
]

export default routes
