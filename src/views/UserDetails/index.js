/* eslint-disable react/prop-types */
import { CContainer, CRow, CCol, CCard, CCardHeader, CCardBody } from '@coreui/react'
import React, { useState, useEffect } from 'react'
import api from 'src/api'
import { useParams } from 'react-router-dom'

const UserDetails = (props) => {
  const [membro, setMembro] = useState(null)

  const { id } = useParams()

  async function getData() {
    // const response = await api.get('star%20wars', { params: id })
    const response = await api.get(`/membro/${id}`)
    console.log(response)
    setMembro(response.data[0])
  }
  useEffect(() => {
    getData()
  }, [])

  if (!membro) return <div>teste</div>
  else
    return (
      <CContainer>
        <CRow>
          <CCol sm="6">
            <CCol>
              <strong>rc</strong>
              <div>{membro.rc}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome Completo</strong>
              <div>{membro.nomeCompleto}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome</strong>
              <div>{membro.nome}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Sobrenome</strong>
              <div>{membro.sobrenome}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome Completo</strong>
              <div>{membro.nomeCompleto}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome</strong>
              <div>{membro.nome}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Sobrenome</strong>
              <div>{membro.sobrenome}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Apelido</strong>
              <div>{membro.apelido}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome do pai</strong>
              <div>{membro.nomePai}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nome da mãe</strong>
              <div>{membro.nomeMae}</div>
            </CCol>
            <br />
            {/* {--- CONTATO ---} */}
            <CCol>
              <strong>Endereço da Residencia</strong>
              <div>{membro.enderecoResidencia}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Telefone Fixo</strong>
              <div>{membro.telFixo}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Celular</strong>
              <div>{membro.celular}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Email Comunitario</strong>
              <div>{membro.emailComunitario}</div>
            </CCol>
            <br />
            {/* {--- DOCUMENTOS ---} */}
            <CCol>
              <strong>RG</strong>
              <div>{membro.rg}</div>
            </CCol>
            <br />
            <CCol>
              <strong>CPF</strong>
              <div>{membro.cpf}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Nascimento</strong>
              <div>{membro.dataNascimento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Falecimento</strong>
              <div>{membro.dataFalecimento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Idade</strong>
              <div>{membro.idade}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Numero do Passaporte</strong>
              <div>{membro.numeroPassaporte}</div>
            </CCol>
            <br />
            {/* {--- COMUNIDADE ---} */}
            <CCol>
              <strong>Ano de Entrada</strong>
              <div>{membro.anoEntrada}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Entrada</strong>
              <div>{membro.dataEntrada}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Tempo de Comunidade</strong>
              <div>{membro.tempoComunidade}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Desligamento</strong>
              <div>{membro.dataDesligamento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Ano de Desligamento</strong>
              <div>{membro.anoDesligamento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Documento do Desligamento</strong>
              <div>{membro.docDesligamento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Chegada na Missão Atual</strong>
              <div>{membro.chegadaMissaoAtual}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Tempo na Missão Atual (Meses)</strong>
              <div>{membro.tempoMissaoAtualMeses}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Tempo na Missão Atual (Anos)</strong>
              <div>{membro.tempoMissaoAtualAnos}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Situação na Frente de Missão Atual</strong>
              <div>{membro.situacaoFrenteMissaoAtual}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Ingresso no Juniorato</strong>
              <div>{membro.dataIngressoJuniorato}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Ingresso Temporario</strong>
              <div>{membro.dataIngressoTemporario}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Ingresso Definitivo</strong>
              <div>{membro.dataIngressoDefinitivo}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Data de Compromisso Definitivo</strong>
              <div>{membro.dataCompromissoDefinitivo}</div>
            </CCol>
            <br />
            {/* {=== RELAÇÕES ===} */}
            <CCol>
              <strong>Status</strong>
              <div>{membro.status}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Titulo</strong>
              <div>{membro.titulo}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Sexo</strong>
              <div>{membro.sexo}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Estado Civil</strong>
              <div>{membro.estadoCivil}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Estado Canônico</strong>
              <div>{membro.estadoCanonico}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Estado Condição de Vida</strong>
              <div>{membro.estadoCondicaoVida}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Etapa Condição de Vida</strong>
              <div>{membro.etapaCondicaoVida}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Cidade Residencia</strong>
              <div>{membro.cidadeResidencia}</div>
            </CCol>
            <br />
            {/* {--- DOCUMENTOS ---} */}
            <CCol>
              <strong>Habilitação</strong>
              <div>{membro.habilitacao}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Grau de Instrução</strong>
              <div>{membro.grauInstrucao}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Profissão</strong>
              <div>{membro.profissao}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Grupo Etário</strong>
              <div>{membro.grupoEtario}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Local de Nascimento</strong>
              <div>{membro.localNascimento}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Nacionalidade</strong>
              <div>{membro.nacionalidade}</div>
            </CCol>
            <br />
            <CCol>
              <strong>País</strong>
              <div>{membro.pais}</div>
            </CCol>
            <br />
            {/* {--- COMUNIDADE ---} */}
            <CCol>
              <strong>Afastamento Temporario</strong>
              <div>{membro.afastamentoTemporario}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Modo de Compromisso</strong>
              <div>{membro.modoCompromisso}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Pertença</strong>
              <div>{membro.pertenca}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Grau de Pertença</strong>
              <div>{membro.grauPertenca}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Frente de Missão</strong>
              <div>{membro.frenteMissao}</div>
            </CCol>
            <br />
            <CCol>
              <strong>Residencia Comunitária</strong>
              <div>{membro.residenciaComunitaria}</div>
            </CCol>
            <br />
          </CCol>
        </CRow>
      </CContainer>
    )
}
export default UserDetails
