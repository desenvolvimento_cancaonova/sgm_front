/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'

import {
  CAvatar,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilPeople, cifUs, cifPt } from '@coreui/icons'

import avatar1 from './../../assets/images/avatars/1.jpg'
import avatar2 from './../../assets/images/avatars/2.jpg'

import api from './../../api'

const Users = ({ history }, props) => {
  const [testes, setTestes] = useState([])
  const avatares = [
    avatar1,
    avatar2,
    'https://www.memesmonkey.com/images/memesmonkey/57/57ac9434ab234a4e37406a8cc6ac309a.jpeg',
    'https://cdn.discordapp.com/avatars/402817082506608640/2343080f77065e05c528cafaf304d8dd.webp?size=80',
  ]
  const bandeiras = [cifUs, cifPt]

  async function getData() {
    // const response = await api.get('star%20wars')
    const response = await api.get('/membro')

    setTestes(response.data)
  }
  useEffect(() => {
    getData()
  }, [])

  return (
    <div>
      <CTable hover responsive align="middle" className="mb-0 border">
        <CTableHead color="light">
          <CTableRow>
            <CTableHeaderCell className="text-center">
              <CIcon icon={cilPeople} />
            </CTableHeaderCell>
            <CTableHeaderCell>User</CTableHeaderCell>
            <CTableHeaderCell className="text-center">Country</CTableHeaderCell>
          </CTableRow>
        </CTableHead>
        <CTableBody>
          {testes.map((membro, index) => {
            console.log(membro)
            return (
              <CTableRow
                title={membro.id}
                key={index}
                onClick={() => history.push(`/user-details/${membro.id}`)}
              >
                <CTableDataCell className="text-center">
                  <CAvatar size="lg" src={avatares[index % avatares.length]} status="success" />
                </CTableDataCell>
                <CTableDataCell>
                  <div>{membro.nomeCompleto}</div>
                  <div className="small text-medium-emphasis">{membro.cpf}</div>
                </CTableDataCell>

                <CTableDataCell className="text-center">
                  <CIcon size="xl" icon={bandeiras[index % bandeiras.length]} title="us" id="us" />
                </CTableDataCell>
              </CTableRow>
            )
          })}
        </CTableBody>
      </CTable>
    </div>
  )
}
export default Users
