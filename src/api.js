/* eslint-disable prettier/prettier */
import axios from 'axios'

const api = axios.create({
  // baseURL: 'https://api.tvmaze.com/search/shows?q='
  baseURL: 'http://localhost:3001'
})
export default api
